"""Celery periodic tasks"""
from logging import getLogger
from celery.schedules import crontab
from src.server import CELERY


@CELERY.task
def sample(arg):
    """Sample task"""
    logger = getLogger()
    logger.info(arg)


@CELERY.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    """Sample periodic tasks"""
    sender.add_periodic_task(10.0, sample.s('start'), name='every 10s')
    sender.add_periodic_task(
        crontab(hour=0, minute=0, day_of_week=1),
        sample.s('Run every monday!'),
    )
