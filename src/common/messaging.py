"""Send mail"""
from logging import getLogger
import jinja2
from flask import current_app
from sendgrid.helpers.mail import Mail
from sendgrid import SendGridAPIClient
from src.server import CELERY


def send_mail_internal(
        api_key,
        to_emails,
        subject,
        template):
    """Send mail"""
    try:
        getLogger().info('Sending mail ...')
        template_loader = jinja2.FileSystemLoader(
            searchpath=template['template']['location'])
        template_env = jinja2.Environment(
            loader=template_loader, autoescape=True)
        html_content = template_env.get_template(
            template['template']['name']
        ).render(data=template['template']['data'])

        getLogger().info('Rendering mail ...')

        message = Mail(
            from_email='sanchanm@wekancode.com',
            to_emails=to_emails,
            subject=subject,
            html_content=html_content)
        sendgrid = SendGridAPIClient(api_key)
        sendgrid.send(message)

        getLogger().info('Mail Sent')
    except Exception as error:  # pylint: disable=broad-except
        getLogger().error(
            "%s Failed to send verification email to %s", str(error.__class__), to_emails)


@CELERY.task()
def send_mail(to_emails, subject, template_location, template, template_data):
    """Send mail celery task"""
    getLogger().info('Triggering mail task ...')
    send_mail_internal(
        current_app.config['SECRETS']['sendGridApiKey'],
        to_emails,
        subject,
        {'template': {'location': template_location,
                      'name': template,
                      'data': template_data}})
