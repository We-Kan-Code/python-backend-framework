"""AWS functions for configuration manager"""
import json
import boto3
from botocore.exceptions import ClientError


def get_secrets(secret_region, secret_name):
    """Get secrets needed to bootstrap the app from SM"""
    secrets = None

    client = boto3.session.Session().client(
        service_name='secretsmanager', region_name=secret_region)

    try:
        secrets_response = client.get_secret_value(
            SecretId=secret_name)
    except ClientError as error:
        print(error)
    else:
        if 'SecretString' in secrets_response:
            print('[Boot] Successfully retrieved secrets')
            secrets = json.loads(secrets_response['SecretString'])
    return secrets
