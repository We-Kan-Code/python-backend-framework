"""Configuration manager"""
import os
import sys
import logging
from dotenv import load_dotenv, find_dotenv
from src.configuration.modules.aws import get_secrets
from src.common.constants import ENV_PATH
from src.common.exceptions import ConfigurationError
import src.resources.strings.configuration as strings


def get_configuration(logger):
    """Loads configuration from .env and aws secrets manager"""
    try:
        def configuration():
            return None

        if not find_dotenv(ENV_PATH):
            raise ConfigurationError(strings.ENV_FILE_NOT_FOUND)

        load_dotenv(ENV_PATH, verbose=True)

        configuration.NAME = os.getenv('NAME')
        # Flask debug + testing
        configuration.DEBUG = (os.getenv('FLASK_DEBUG') == 'True')
        configuration.TESTING = (os.getenv('TESTING') == 'True')
        # Auth
        configuration.AUTH_TOKEN_EXPIRY = os.getenv('AUTH_TOKEN_EXPIRY')
        # Logging
        configuration.LOG_CONSOLE_LEVEL = os.getenv('LOG_CONSOLE_LEVEL')
        configuration.LOG_DEBUG_FILE_LEVEL = os.getenv('LOG_DEBUG_FILE_LEVEL')
        configuration.LOG_DEBUG_FILE_TO = os.getenv('LOG_DEBUG_FILE_TO')
        configuration.LOG_ERROR_FILE_LEVEL = os.getenv('LOG_ERROR_FILE_LEVEL')
        configuration.LOG_ERROR_FILE_TO = os.getenv('LOG_ERROR_FILE_TO')
        # Default user profile image
        configuration.DEFAULT_PROFILE_IMAGE_URL = os.getenv(
            'DEFAULT_PROFILE_IMAGE_URL')
        # AWS secrets manager
        configuration.AWS_SECRETS_NAME = os.getenv('AWS_SECRETS_NAME')
        configuration.AWS_SECRETS_REGION = os.getenv('AWS_SECRETS_REGION')
        # SQLAlchemy
        configuration.SQLALCHEMY_DATABASE_URI = os.getenv(
            'SQLALCHEMY_DATABASE_URI')
        configuration.SQLALCHEMY_TRACK_MODIFICATIONS = (os.getenv(
            'SQLALCHEMY_TRACK_MODIFICATIONS') == 'True')
        # Celery
        configuration.CELERY_BROKER_URL = os.getenv('CELERY_BROKER_URL')
        configuration.CELERY_BACKEND_URL = os.getenv('CELERY_BACKEND_URL')

        secret_region = configuration.AWS_SECRETS_REGION
        secret_name = configuration.AWS_SECRETS_NAME

        if not secret_region:
            raise ConfigurationError(strings.AWS_REGION_NAME_NOT_FOUND)

        if not secret_name:
            raise ConfigurationError(strings.AWS_SECRET_NAME_NOT_FOUND)

        configuration.SECRETS = get_secrets(secret_region, secret_name)
        configuration.SECRET_KEY = configuration.SECRETS['authSecret']

        return configuration
    except ConfigurationError as error:
        logger.fatal('[Boot] ' + getattr(error, 'message', str(error)))
        sys.exit(1)


def setup_configuration(app):
    """Configures the app"""
    logger = logging.getLogger(__name__)
    configuration = get_configuration(logger)
    app.config.from_object(configuration)
