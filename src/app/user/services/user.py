"""User service"""
from http import HTTPStatus as http_status
from flask import request, current_app as app
from marshmallow import EXCLUDE
from src.app.user.models.user import User as UserModel
from src.app.user.schema.user import UserSchema, RoleSchema
from src.common.exceptions import ApiError
from src.common.helpers import (
    to_snake_case,
    validate_sparse_fieldsets,
    remove_empty_objects,
    build_pagination_meta)
from src.common.messaging import send_mail
import src.common.response as response
import src.resources.strings.user as strings


class UserService:
    """User service class"""

    def __init__(self):
        self.user_schema = UserSchema(unknown=EXCLUDE)
        self.users_schema = UserSchema(many=True)
        self.relations = {'role': RoleSchema}

    # Helper methods
    @staticmethod
    def send_register_mail(user):
        """Triggers the user creation mail containing the verification code"""
        template_data = {
            'first_name': user['firstName'],
            'service_name': app.config['NAME'],
            'verification_code': user['verificationCode']
        }
        send_mail.delay(
            user['email'],
            strings.SIGNUP_EMAIL_SUBJECT,
            './src/app/user/templates/email',
            'verify-account.html',
            template_data)

    # CRUD operations
    def create_one(self, request_body):
        """
        Create a single user if there are no existing users that have the
        same email address.

        On successful user creation, the user status is set to NOT_VERIFIED
        and an email with a verification code is sent to the registered email
        address.
        """
        user_data = self.user_schema.load(request_body)

        user = UserModel.get_by_email(user_data.get('email'))
        if user:
            raise ApiError(http_status.CONFLICT, strings.CONFLICT_ERROR)

        user = UserModel(user_data)
        user.save()
        user = self.user_schema.dump(user)
        UserService.send_register_mail(user)
        user.pop('verificationCode', None)

        return response.success(http_status.CREATED, 'user', user)

    def update_one(self, user_id, request_body):
        """
        Updates the user if the user exists. If a new password is provided it is
        hashed, in the user.update call, before they are updated
        """
        user_data = self.user_schema.load(request_body, partial=True)
        user_data.pop('email', None)

        user = UserModel.get_by_id(user_id)
        if not user:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        user.update(user_data)
        user = self.user_schema.dump(user)

        return response.success(http_status.OK, 'user', user)

    def get_one(self, user_id, fields, relations):
        """
        Get a single user based on the provided user_id

        Returns a specific user object.

        fields = Comma separated list of fields should be returned.

        relations = Relation and relation attributes that must be returned
        """
        valid_fields = set(UserModel.__dict__)
        fields = to_snake_case(fields).split(',')
        fields_to_return = [*(valid_fields & set(fields))]
        user_schema = UserSchema(
            only=validate_sparse_fieldsets(
                fields_to_return, relations, self.relations))

        user = UserModel.get_one(user_id)
        if user is None:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        user = user_schema.dump(user)

        return response.success(http_status.OK, 'user', user)

    def get_many(self, fields, relations, sort, page, per_page):
        """
        Get many / all users

        Returns an array of users.

        fields = Comma separated list of fields should be returned.

        relations = Relation and relation attributes that must be returned

        sort = Sorts the results based on the provided column in asc/desc order.
        Defaults to 'id|asc', sort by id in ascending order, if sort if None or invalid

        page = Provides pagination. Starts at 1. If zero (also the default if no value
        is provided), pagination is disabled and all results are returned

        per_page = No. of results to be returned in each page, defaults to 10
        """
        valid_fields = set(UserModel.__dict__)
        fields = to_snake_case(fields).split(',')
        fields_to_return = [*(valid_fields & set(fields))]
        original_users_schema = self.users_schema
        new_users_schema = UserSchema(
            many=True,
            only=validate_sparse_fieldsets(
                fields_to_return, relations, self.relations))
        self.users_schema = new_users_schema

        sort[0] = to_snake_case(sort[0])
        if sort[0] not in valid_fields:
            sort[0] = 'id'
            sort[1] = 'asc'

        users = UserModel.get_many(
            sort=sort,
            page=page,
            per_page=per_page)

        meta = None

        if page > 0:
            meta = build_pagination_meta(users, request)
            users = remove_empty_objects(users.items, self.users_schema)
        else:
            users = remove_empty_objects(users, self.users_schema)

        self.users_schema = original_users_schema

        return response.success(http_status.OK, 'users', users, meta)

    @staticmethod
    def delete_one(user_id):
        """Checks if the user exists and deletes it"""
        user = UserModel.query.get(user_id)
        if not user:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        UserModel.delete_one(user_id)

        return response.success(http_status.NO_CONTENT)
