"""User endpoints"""
from flask import Blueprint, request
from flask_jwt_extended import jwt_required
from marshmallow import ValidationError
from sqlalchemy import exc
from argon2.exceptions import HashingError
from src.app.user.services.user import UserService
from src.common.exceptions import ApiError
from src.common.constants import (
    DEFAULT_PAGE_LIMIT,
    DISABLE_PAGINATION
)
from src.middleware.access_control import access_control
import src.common.response as response

USER_API = Blueprint('users', __name__)


@USER_API.route('/users', methods=['POST'])
def create():
    """Create a single user"""
    try:
        request_body = request.get_json()
        return UserService().create_one(request_body)
    except ValidationError as error:
        return response.validation_error(error)
    except (ApiError, ValueError, TypeError, HashingError, exc.SQLAlchemyError) as error:
        return response.error(error)


def user_update_handler(identity, user):
    """
    Custom access control handler
    Allow user to update only his own information
    """
    if identity['id'] == request.view_args['user_id']:
        return True
    return False


@USER_API.route('/users/<user_id>', methods=['PATCH'])
@jwt_required
@access_control(custom_handler=user_update_handler)
def update(user_id):
    """Update a single user by id"""
    try:
        request_body = request.get_json()
        return UserService().update_one(user_id, request_body)
    except ValidationError as error:
        return response.validation_error(error)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@USER_API.route('/users/<user_id>', methods=['GET'])
@jwt_required
@access_control()
def get_one(user_id):
    """Get a single user by id"""
    try:
        fields = request.args.get(
            'fields',
            # Default fields to return
            'id, first_name, last_name, email, status')
        relations = request.args.get('expand', '')\
            .replace('}', '')\
            .replace(' ', '')\
            .split('|')
        return UserService().get_one(user_id, fields, relations)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@USER_API.route('/users', methods=['GET'])
@jwt_required
@access_control()
def get_many():
    """Get many / all users"""
    try:
        page = int(request.args.get('page', DISABLE_PAGINATION))
        sort = request.args.get('sort', 'id|asc').split('|')
        per_page = int(request.args.get('per_page', DEFAULT_PAGE_LIMIT))
        fields = request.args.get(
            'fields',
            # Default fields to return
            'id, first_name, last_name, email, status')
        relations = request.args.get('expand', '')\
            .replace('}', '')\
            .replace(' ', '')\
            .split('|')
        return UserService().get_many(fields, relations, sort, page, per_page)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@USER_API.route('/users/<user_id>', methods=['DELETE'])
@jwt_required
@access_control()
def delete_one(user_id):
    """Delete a single user by id"""
    try:
        return UserService.delete_one(user_id)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)
