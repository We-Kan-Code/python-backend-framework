"""User permissions endpoints"""
from flask import (request, Blueprint)
from flask_jwt_extended import jwt_required
from marshmallow import ValidationError
from sqlalchemy import exc
from src.common.constants import (
    DEFAULT_PAGE_LIMIT,
    DISABLE_PAGINATION
)
from src.app.auth.services.permission import PermissionService
from src.middleware.access_control import access_control
from src.common.exceptions import ApiError
import src.common.response as response

AUTH_PERMISSION_API = Blueprint('permissions', __name__)


@AUTH_PERMISSION_API.route('/permissions', methods=['POST'])
@jwt_required
@access_control()
def create():
    """Create a permission for the provided role"""
    try:
        return PermissionService().create_one(request.get_json())
    except ValidationError as error:
        return response.validation_error(error)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_PERMISSION_API.route('/permissions/<permission_id>', methods=['PATCH'])
@jwt_required
@access_control()
def update(permission_id):
    """Update a permission by id"""
    try:
        return PermissionService().update_one(permission_id, request.get_json())
    except ValidationError as error:
        return response.validation_error(error)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_PERMISSION_API.route('/permissions/<permission_id>', methods=['GET'])
@jwt_required
@access_control()
def get_one(permission_id):
    """Get a single permission by id"""
    try:
        fields = request.args.get(
            'fields',
            # Default fields to return
            'id, name, permissions, status')
        relations = request.args.get('expand', '')\
            .replace('}', '')\
            .replace(' ', '')\
            .split('|')
        return PermissionService().get_one(permission_id, fields, relations)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_PERMISSION_API.route('/permissions', methods=['GET'])
@jwt_required
@access_control()
def get_many():
    """Get many / all permissions"""
    try:
        page = int(request.args.get('page', DISABLE_PAGINATION))
        sort = request.args.get('sort', 'id|asc').split('|')
        per_page = int(request.args.get('per_page', DEFAULT_PAGE_LIMIT))
        fields = request.args.get(
            'fields',
            # Default fields to return
            'id, name, permissions, status')
        relations = request.args.get('expand', '')\
            .replace('}', '')\
            .replace(' ', '')\
            .split('|')
        return PermissionService().get_many(fields, relations, sort, page, per_page)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_PERMISSION_API.route('/permissions/<permission_id>', methods=['DELETE'])
@jwt_required
@access_control()
def delete_one(permission_id):
    """Delete a single permission by id"""
    try:
        return PermissionService.delete_one(permission_id)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)
