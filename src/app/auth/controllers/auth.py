"""Auth endpoints"""
from http import HTTPStatus as http_status
from flask import (request, Blueprint)
from flask_jwt_extended import (
    get_jwt_identity,
    get_raw_jwt,
    jwt_refresh_token_required
)
from marshmallow import ValidationError
from sqlalchemy import exc
from argon2.exceptions import (
    VerificationError,
    VerifyMismatchError,
    InvalidHash
)
from src.app.auth.services.auth import AuthService
from src.common.exceptions import ApiError
import src.common.response as response

AUTH_USER_API = Blueprint('auth', __name__)


@AUTH_USER_API.route('/auth/users/login', methods=['POST'])
def login():
    """
    On successful login, returns user information along with access and
    refresh tokens.
    """
    try:
        return AuthService().auth(request.get_json())
    except ValidationError as error:
        return response.validation_error(error)
    except (VerificationError, VerifyMismatchError) as error:
        return response.error(error, http_status.UNAUTHORIZED)
    except (ApiError, InvalidHash, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_USER_API.route('/auth/users/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    """
    Refresh access tokens.

    Checks to see if the refresh token is valid and if it is,
    generates and returns the new access token.
    """
    try:
        refresh_token_jti = get_raw_jwt().get('jti')
        email = get_jwt_identity()['email']
        return AuthService().refresh_tokens(email, refresh_token_jti)
    except (ApiError, KeyError, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_USER_API.route('/auth/users/reset-password', methods=['POST'])
def reset_password():
    """
    Creates a temporary password, sets status to FORCE_RESET_PASSWORD
    and then mails it to the user.

    On logging in with the temporary password the user can then be
    forced to update their password.
    """
    try:
        return AuthService().reset_password(request.get_json())
    except ValidationError as error:
        return response.validation_error(error)
    except (ApiError, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_USER_API.route('/auth/users/<user_id>/verification-code', methods=['POST'])
def get_verification_code(user_id):
    """Resends the verification code to the provided email"""
    try:
        return AuthService().send_verification_code(user_id)
    except (ApiError, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_USER_API.route('/auth/users/<user_id>/verify', methods=['POST'])
def verify_code(user_id):
    """
    Verifies the user based on the code provided.

    If the code is correct, user status is set to VERIFIED and then
    access and refresh tokens are generated and sent back to facilitate
    login.
    """
    try:
        return AuthService().verify_account(user_id, request.get_json())
    except ValidationError as error:
        return response.validation_error(error)
    except (VerificationError, VerifyMismatchError) as error:
        return response.error(error, http_status.UNAUTHORIZED)
    except (ApiError, InvalidHash, exc.SQLAlchemyError) as error:
        return response.error(error)
