"""User roles endpoints"""
from flask import (request, Blueprint)
from flask_jwt_extended import jwt_required
from marshmallow import ValidationError
from sqlalchemy import exc
from src.common.constants import (
    DEFAULT_PAGE_LIMIT,
    DISABLE_PAGINATION
)
from src.app.auth.services.role import RoleService
from src.middleware.access_control import access_control
from src.common.exceptions import ApiError
import src.common.response as response

AUTH_ROLE_API = Blueprint('roles', __name__)


@AUTH_ROLE_API.route('/roles', methods=['POST'])
@jwt_required
@access_control()
def create():
    """Create a single role"""
    try:
        return RoleService().create_one(request.get_json())
    except ValidationError as error:
        return response.validation_error(error)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_ROLE_API.route('/roles/<role_id>', methods=['PATCH'])
@jwt_required
@access_control()
def update(role_id):
    """Update a role by id"""
    try:
        return RoleService().update_one(role_id, request.get_json())
    except ValidationError as error:
        return response.validation_error(error)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_ROLE_API.route('/roles/<role_id>', methods=['GET'])
@jwt_required
@access_control()
def get_one(role_id):
    """Get a single role by id"""
    try:
        fields = request.args.get(
            'fields',
            # Default fields to return
            'id, name, status')
        relations = request.args.get('expand', '')\
            .replace('}', '')\
            .replace(' ', '')\
            .split('|')
        return RoleService().get_one(role_id, fields, relations)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_ROLE_API.route('/roles', methods=['GET'])
@jwt_required
@access_control()
def get_many():
    """Get many / all roles"""
    try:
        page = int(request.args.get('page', DISABLE_PAGINATION))
        sort = request.args.get('sort', 'id|asc').split('|')
        per_page = int(request.args.get('per_page', DEFAULT_PAGE_LIMIT))
        fields = request.args.get(
            'fields',
            # Default fields to return
            'id, name, status')
        relations = request.args.get('expand', '')\
            .replace('}', '')\
            .replace(' ', '')\
            .split('|')
        return RoleService().get_many(fields, relations, sort, page, per_page)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@AUTH_ROLE_API.route('/roles/<role_id>', methods=['DELETE'])
@jwt_required
@access_control()
def delete_one(role_id):
    """Delete a single role by id"""
    try:
        return RoleService.delete_one(role_id)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)
