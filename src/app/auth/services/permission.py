"""Permission service"""
from http import HTTPStatus as http_status
from flask import request
from marshmallow import EXCLUDE
from src.app.auth.models.permission import Permission as PermissionModel
from src.app.auth.schema.permission import PermissionSchema
from src.common.helpers import (
    to_snake_case,
    validate_sparse_fieldsets,
    remove_empty_objects,
    build_pagination_meta)
from src.common.exceptions import ApiError
import src.common.response as response
import src.resources.strings.auth as strings


class PermissionService:
    """Permission service class"""

    def __init__(self):
        self.permission_schema = PermissionSchema(unknown=EXCLUDE)
        self.permissions_schema = PermissionSchema(many=True)
        self.relations = {}

    # CRUD operations
    def create_one(self, request_body):
        """Create a single permission."""
        permission_data = self.permission_schema.load(request_body)
        permission = PermissionModel.get_unique(
            permission_data.get('resource'), permission_data.get('action'))
        if permission:
            raise ApiError(http_status.CONFLICT, strings.CONFLICT_ERROR)

        permission = PermissionModel(permission_data)
        permission.save()
        permission = self.permission_schema.dump(permission)

        return response.success(http_status.CREATED, 'permission', permission)

    def update_one(self, permission_id, request_body):
        """Updates the permission if the permission exists."""
        permission_data = self.permission_schema.load(
            request_body, partial=True)
        permission_data.pop('email', None)

        permission = PermissionModel.get_by_id(permission_id)
        if not permission:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        permission.update(permission_data)
        permission = self.permission_schema.dump(permission)

        return response.success(http_status.OK, 'permission', permission)

    def get_one(self, permission_id, fields, relations):
        """
        Get a single permission based on the provided permission_id

        Returns a specific permission object.

        fields = Comma separated list of fields should be returned.

        relations = Relation and relation attributes that must be returned
        """
        valid_fields = set(PermissionModel.__dict__)
        fields = to_snake_case(fields).split(',')
        fields_to_return = [*(valid_fields & set(fields))]
        permission_schema = validate_sparse_fieldsets(
            fields_to_return, relations, self.relations)

        permission = PermissionModel.get_one(permission_id, fields_to_return)
        if permission is None:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        permission = permission_schema.dump(permission)

        return response.success(http_status.OK, 'permission', permission)

    def get_many(self, fields, relations, sort, page, per_page):
        """
        Get many / all permissions

        Returns an array of permissions.

        fields = Comma separated list of fields should be returned.

        relations = Relation and relation attributes that must be returned

        sort = Sorts the results based on the provided column in asc/desc order.
        Defaults to 'id|asc', sort by id in ascending order, if sort if None or invalid

        page = Provides pagination. Starts at 1. If zero (also the default if no value
        is provided), pagination is disabled and all results are returned

        per_page = No. of results to be returned in each page, defaults to 10
        """
        valid_fields = set(PermissionModel.__dict__)
        fields = to_snake_case(fields).split(',')
        fields_to_return = [*(valid_fields & set(fields))]
        original_permissions_schema = self.permissions_schema
        new_permissions_schema = PermissionSchema(
            many=True,
            only=validate_sparse_fieldsets(
                fields_to_return, relations, self.relations))
        self.permissions_schema = new_permissions_schema

        sort[0] = to_snake_case(sort[0])
        if sort[0] not in valid_fields:
            sort[0] = 'id'
            sort[1] = 'asc'

        permissions = PermissionModel.get_many(
            columns=fields_to_return,
            sort=sort,
            page=page,
            per_page=per_page)

        meta = None

        if page > 0:
            meta = build_pagination_meta(permissions, request)
            permissions = remove_empty_objects(
                permissions.items, self.permissions_schema)
        else:
            permissions = remove_empty_objects(
                permissions, self.permissions_schema)

        self.permissions_schema = original_permissions_schema

        return response.success(http_status.OK, 'permissions', permissions, meta)

    @staticmethod
    def delete_one(permission_id):
        """Checks if the permission exists and deletes it"""
        permission = PermissionModel.query.get(permission_id)
        if not permission:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        PermissionModel.delete_one(permission_id)

        return response.success(http_status.NO_CONTENT)
