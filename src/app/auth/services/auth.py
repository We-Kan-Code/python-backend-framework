"""Auth service"""
from http import HTTPStatus as http_status
from hashlib import sha512
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    decode_token)
from marshmallow import EXCLUDE
from src.app.user.models.user import User as UserModel
from src.app.auth.schema.auth import LoginSchema, CommonSchema
from src.common.constants import (
    TMP_PASSWORD_LENGTH,
    USER_FORCE_RESET_PASSWORD,
    USER_VERIFIED,
    USER_NOT_VERIFIED)
from src.common.exceptions import ApiError
from src.common.helpers import generate_random_string
from src.common.messaging import send_mail
import src.common.response as response
from src.resources.strings.auth import (
    INCORRECT_CREDENTIALS,
    INVALID_REFRESH_TOKEN,
    NOT_FOUND_ERROR,
    RESET_PASSWORD_EMAIL_SUBJECT,
    SEND_VERIFICATION_CODE_EMAIL_SUBJECT,
    USER_ALREADY_VERIFIED,
    INVALID_VERIFICATION_CODE)


class AuthService:
    """Auth service class"""

    def __init__(self):
        self.login_schema = LoginSchema(unknown=EXCLUDE)
        self.common_schema = CommonSchema(unknown=EXCLUDE)

    def auth(self, request_body):
        """
        If the provided credentials are valid, returns access and
        refresh tokens
        """
        login_data = self.login_schema.load(request_body)
        email = login_data.get('email')
        fcm_token = login_data.get('fcm_token', None)

        user = UserModel.get_by_email(email)
        if not user:
            raise ApiError(http_status.UNAUTHORIZED, INCORRECT_CREDENTIALS)

        # Argon2's verify raises a VerifyMismatchError or
        # VerificationError if the password hash check fails
        user.check_hash(login_data.get('password'))

        user.tmp_password = None
        access_token = create_access_token(
            identity={'id': user.id, 'email': email})
        refresh_token = create_refresh_token(
            identity={'id': user.id, 'email': email})
        user.refresh_token = refresh_token
        user.fcm_token = fcm_token
        if user.verification_code:
            user.status = USER_NOT_VERIFIED
        else:
            user.status = USER_VERIFIED
        user.save()

        user = self.login_schema.dump(user)
        user['accessToken'] = access_token
        user['refreshToken'] = refresh_token

        return response.success(http_status.OK, 'user', user)

    def refresh_tokens(self, email, refresh_token_jti):
        """
        If the provided refresh token is valid, returns new access and
        refresh tokens. Invalidates older refresh tokens.
        """
        user_instance = UserModel.get_by_email(email)
        if not user_instance:
            raise ApiError(http_status.UNAUTHORIZED, INCORRECT_CREDENTIALS)

        user = self.login_schema.dump(user_instance)

        stored_refresh_token_jti = decode_token(
            user['refreshToken']).get('jti')

        if refresh_token_jti != stored_refresh_token_jti:
            raise ApiError(http_status.UNAUTHORIZED, INVALID_REFRESH_TOKEN)

        access_token = create_access_token(
            identity={'id': user['id'], 'email': email})
        refresh_token = create_refresh_token(
            identity={'id': user['id'], 'email': email})
        user_instance.refresh_token = refresh_token
        user_instance.save()

        user['accessToken'] = access_token
        user['refreshToken'] = refresh_token

        return response.success(http_status.OK, 'user', user)

    def reset_password(self, request_body):
        """
        Generates temporary password and mails it tot the user. Becomes
        invalid after logging in with the temporary or the original password.

        If the user logs in with the temporary password, the user's status is
        set to USER_FORCE_RESET_PASSWORD
        """
        email = self.common_schema.load(request_body)['email']

        user = UserModel.get_by_email(email)
        if not user:
            raise ApiError(http_status.NOT_FOUND, NOT_FOUND_ERROR)

        tmp_password = generate_random_string(TMP_PASSWORD_LENGTH)
        tmp_password_hash = sha512(tmp_password.encode()).hexdigest()
        user.update({'tmp_password': tmp_password_hash,
                     'status': USER_FORCE_RESET_PASSWORD})
        user = self.common_schema.dump(user)

        send_mail.delay(
            user['email'],
            RESET_PASSWORD_EMAIL_SUBJECT,
            './src/app/auth/templates/email',
            'reset-password.html',
            {
                'first_name': user['firstName'],
                'tmp_password': tmp_password,
            })

        return response.success(http_status.NO_CONTENT)

    def send_verification_code(self, user_id):
        """Send the code used to verify the user's account"""
        user = UserModel.get_by_id(user_id)
        if not user:
            raise ApiError(http_status.NOT_FOUND, NOT_FOUND_ERROR)

        user = self.common_schema.dump(user)

        send_mail.delay(
            user['email'],
            SEND_VERIFICATION_CODE_EMAIL_SUBJECT,
            './src/app/auth/templates/email',
            'send-verification-code.html',
            {
                'first_name': user['firstName'],
                'verification_code': user['verificationCode'],
            })

        return response.success(http_status.NO_CONTENT)

    def verify_account(self, user_id, request_body):
        """
        Verify user's account after first login. On verification, access and refresh
        tokens are provided to facilitate login
        """
        login_data = self.login_schema.load(request_body, partial=True)

        provided_verification_code = login_data.get('verification_code', None)

        user = UserModel.get_by_id(user_id)
        if not user:
            raise ApiError(http_status.NOT_FOUND, NOT_FOUND_ERROR)

        if user.status == USER_VERIFIED:
            raise ApiError(http_status.NOT_FOUND,
                           USER_ALREADY_VERIFIED)

        if user.verification_code != provided_verification_code:
            raise ApiError(http_status.BAD_REQUEST,
                           INVALID_VERIFICATION_CODE)

        user.verification_code = None
        user.status = USER_VERIFIED
        access_token = create_access_token(
            identity={'id': user.id, 'email': user.email})
        refresh_token = create_refresh_token(
            identity={'id': user.id, 'email': user.email})
        user.refresh_token = refresh_token
        user.fcm_token = None
        user.save()

        user = self.login_schema.dump(user)
        user['accessToken'] = access_token
        user['refreshToken'] = refresh_token

        return response.success(http_status.OK, 'user', user)
