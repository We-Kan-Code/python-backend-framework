"""Role service"""
from http import HTTPStatus as http_status
from flask import request
from marshmallow import EXCLUDE
from src.app.auth.models.role import Role as RoleModel
from src.app.auth.schema.permission import PermissionSchema
from src.app.auth.schema.role import RoleSchema, UserSchema
from src.common.helpers import (
    to_snake_case,
    validate_sparse_fieldsets,
    remove_empty_objects,
    build_pagination_meta)
from src.common.exceptions import ApiError
import src.common.response as response
import src.resources.strings.auth as strings


class RoleService:
    """Role service class"""

    def __init__(self):
        self.role_schema = RoleSchema(unknown=EXCLUDE)
        self.roles_schema = RoleSchema(many=True)
        self.relations = {'permissions': PermissionSchema, 'users': UserSchema}

    # CRUD operations
    def create_one(self, request_body):
        """Create a single role."""
        role_data = self.role_schema.load(request_body)
        role = RoleModel.get_by_name(role_data.get('name'))
        if role:
            raise ApiError(http_status.CONFLICT, strings.CONFLICT_ERROR)

        role = RoleModel(role_data)
        role.save()
        role = self.role_schema.dump(role)

        return response.success(http_status.CREATED, 'role', role)

    def update_one(self, role_id, request_body):
        """Updates the role if the role exists."""
        role_data = self.role_schema.load(request_body, partial=True)
        role_data.pop('email', None)

        role = RoleModel.get_by_id(role_id)
        if not role:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        role.update(role_data)
        role = self.role_schema.dump(role)

        return response.success(http_status.OK, 'role', role)

    def get_one(self, role_id, fields, relations):
        """
        Get a single role based on the provided role_id

        Returns a specific role object.

        fields = Comma separated list of fields should be returned.

        relations = Relation and relation attributes that must be returned
        """
        valid_fields = set(RoleModel.__dict__)
        fields = to_snake_case(fields).split(',')
        fields_to_return = [*(valid_fields & set(fields))]
        role_schema = validate_sparse_fieldsets(
            fields_to_return, relations, self.relations)

        role = RoleModel.get_one(role_id)
        if role is None:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        role = role_schema.dump(role)

        return response.success(http_status.OK, 'role', role)

    def get_many(self, fields, relations, sort, page, per_page):
        """
        Get many / all roles

        Returns an array of roles.

        fields = Comma separated list of fields should be returned.

        relations = Relation and relation attributes that must be returned

        sort = Sorts the results based on the provided column in asc/desc order.
        Defaults to 'id|asc', sort by id in ascending order, if sort if None or invalid

        page = Provides pagination. Starts at 1. If zero (also the default if no value
        is provided), pagination is disabled and all results are returned

        per_page = No. of results to be returned in each page, defaults to 10
        """
        valid_fields = set(RoleModel.__dict__)
        fields = to_snake_case(fields).split(',')
        fields_to_return = [*(valid_fields & set(fields))]
        original_roles_schema = self.roles_schema
        new_roles_schema = RoleSchema(
            many=True,
            only=validate_sparse_fieldsets(
                fields_to_return, relations, self.relations))
        self.roles_schema = new_roles_schema

        sort[0] = to_snake_case(sort[0])
        if sort[0] not in valid_fields:
            sort[0] = 'id'
            sort[1] = 'asc'

        roles = RoleModel.get_many(
            sort=sort,
            page=page,
            per_page=per_page)

        meta = None

        if page > 0:
            meta = build_pagination_meta(roles, request)
            roles = remove_empty_objects(roles.items, self.roles_schema)
        else:
            roles = remove_empty_objects(roles, self.roles_schema)

        self.roles_schema = original_roles_schema

        return response.success(http_status.OK, 'roles', roles, meta)

    @staticmethod
    def delete_one(role_id):
        """Checks if the role exists and deletes it"""
        role = RoleModel.query.get(role_id)
        if not role:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        RoleModel.delete_one(role_id)

        return response.success(http_status.NO_CONTENT)
