"""Celery worker entry point"""
from src.server import create_app

APP, CELERY = create_app()
APP.app_context().push()
