"""App startup"""
import os
from celery import Celery
from flask import Flask
from flask_restful import Api
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from src.configuration.manager import setup_configuration
from src.configuration.modules.auth import setup_auth
from src.configuration.modules.logger import setup_logger
from src.common.error_handler import (
    register_route_error_handlers,
    register_auth_error_handlers
)
from src.common.logging import setup_request_logger

API = Api()
DB = SQLAlchemy()
BCRYPT = Bcrypt()
JWT = JWTManager()
CELERY = Celery(__name__, broker=os.getenv('TASK_BROKER_URI'))


def make_celery(app):
    """Setup celery"""
    CELERY.conf.update(app.config)

    class ContextTask(CELERY.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    CELERY.Task = ContextTask


def register_blueprints(app, url_prefix):
    """Register blueprints"""
    from src.app.auth.controllers.role import AUTH_ROLE_API as auth_role_blueprint
    from src.app.auth.controllers.permission import AUTH_PERMISSION_API \
        as auth_permissions_blueprint
    from src.app.user.controllers.user import USER_API as user_blueprint
    from src.app.auth.controllers.auth import AUTH_USER_API as auth_user_blueprint
    app.register_blueprint(auth_role_blueprint, url_prefix=url_prefix)
    app.register_blueprint(auth_permissions_blueprint, url_prefix=url_prefix)
    app.register_blueprint(user_blueprint, url_prefix=url_prefix)
    app.register_blueprint(auth_user_blueprint, url_prefix=url_prefix)


def create_app():
    """Initializes app"""
    app = Flask(__name__)
    API.init_app(app)

    setup_configuration(app)
    setup_auth(app)
    setup_logger(app.config)

    app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {
        'hide_parameters': True, 'convert_unicode': False}

    DB.init_app(app)
    BCRYPT.init_app(app)
    JWT.init_app(app)

    # Register routes
    register_blueprints(app, os.getenv('ROUTE_PREFIX'))

    # Register error handlers
    register_route_error_handlers(app)
    register_auth_error_handlers(JWT)

    # Register before/after request callbacks
    app.before_request(setup_request_logger)

    from src.middleware.access_control import filter_attributes
    app.after_request(filter_attributes)

    make_celery(app)

    # Register periodic tasks here
    # import src.common.tasks

    CORS(app, resources={r"/*": {"origins": os.getenv('ROUTE_PREFIX')}})

    return app, CELERY
