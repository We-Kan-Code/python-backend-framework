"""Seed data for permissions"""

READ_ANY = "read:any"
UPDATE_OWN = "update:custom"

SEED_PERMISSIONS = [
    {
        "action": "*:*",
        "attributes": "*",
        "resource": "*",
        "role_id": 1
    },
    {
        "action": READ_ANY,
        "attributes": "*",
        "resource": "roles",
        "role_id": 2
    },
    {
        "action": READ_ANY,
        "attributes": "*",
        "resource": "permissions",
        "role_id": 2
    },
    {
        "action": READ_ANY,
        "attributes": "*",
        "resource": "users",
        "role_id": 2
    },
    {
        "action": UPDATE_OWN,
        "attributes": "*",
        "resource": "users",
        "role_id": 2
    }
]
