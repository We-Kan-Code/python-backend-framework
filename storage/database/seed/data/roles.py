"""Seed data for roles"""

SEED_ROLES = [
    {
        "name": "Administrators",
        "status": 1
    },
    {
        "name": "Regular users",
        "status": 1
    }
]
