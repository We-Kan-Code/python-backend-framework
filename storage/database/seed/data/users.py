"""Seed data for users"""

HASHED_PASSWORD = '122ab7797a6868cc8621f61f8efe98823116134c3' + \
    'a314505e3cdb157e9a84f4900695d58897a7d150cf487ecbe74818a1ed' + \
    '18d39c22218291e13ea632035f3387'

SEED_USERS = [
    {
        "first_name": "Sanchan",
        "last_name": "Moses",
        "email": "admin@wekan.company",
        "password": HASHED_PASSWORD,
        "role_id": 1,
        "status": 1
    }
]
