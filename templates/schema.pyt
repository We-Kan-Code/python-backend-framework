"""%model_upper% schema"""
from marshmallow import fields
from src.common.transformers import CamelCaseSchema

class %model_upper%Schema(CamelCaseSchema):
    """Common %model_lower% schema used for all %model_lower% endpoints"""
    id = fields.Int(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    updated_at = fields.DateTime(dump_only=True)
