"""%model_upper%s endpoints"""
from flask import (request, Blueprint)
from flask_jwt_extended import jwt_required
from marshmallow import ValidationError
from sqlalchemy import exc
from src.common.constants import (
    DEFAULT_PAGE_LIMIT,
    DISABLE_PAGINATION
)
from src.app.%model_lower%.services.%model_lower% import %model_upper%Service
from src.middleware.access_control import access_control
from src.common.exceptions import ApiError
import src.common.response as response

%model_caps%_API = Blueprint('%model_lower%s', __name__)


@%model_caps%_API.route('/%model_lower%s', methods=['POST'])
@jwt_required
@access_control()
def create():
    """Create a single %model_lower%"""
    try:
        return %model_upper%Service().create_one(request.get_json())
    except ValidationError as error:
        return response.validation_error(error)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@%model_caps%_API.route('/%model_lower%s/<%model_lower%_id>', methods=['PATCH'])
@jwt_required
@access_control()
def update(%model_lower%_id):
    """Update a %model_lower% by id"""
    try:
        return %model_upper%Service().update_one(%model_lower%_id, request.get_json())
    except ValidationError as error:
        return response.validation_error(error)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@%model_caps%_API.route('/%model_lower%s/<%model_lower%_id>', methods=['GET'])
@jwt_required
@access_control()
def get_one(%model_lower%_id):
    """Get a single %model_lower% by id"""
    try:
        fields = request.args.get(
            'fields',
            # Default fields to return
            'id, name, status')
        relations = request.args.get('expand', '')\
            .replace('}', '')\
            .replace(' ', '')\
            .split('|')
        return %model_upper%Service().get_one(%model_lower%_id, fields, relations)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@%model_caps%_API.route('/%model_lower%s', methods=['GET'])
@jwt_required
@access_control()
def get_many():
    """Get many / all %model_lower%s"""
    try:
        page = int(request.args.get('page', DISABLE_PAGINATION))
        sort = request.args.get('sort', 'id|asc').split('|')
        per_page = int(request.args.get('per_page', DEFAULT_PAGE_LIMIT))
        fields = request.args.get(
            'fields',
            # Default fields to return
            'id, name, status')
        relations = request.args.get('expand', '')\
            .replace('}', '')\
            .replace(' ', '')\
            .split('|')
        return %model_upper%Service().get_many(fields, relations, sort, page, per_page)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)


@%model_caps%_API.route('/%model_lower%s/<%model_lower%_id>', methods=['DELETE'])
@jwt_required
@access_control()
def delete_one(%model_lower%_id):
    """Delete a single %model_lower% by id"""
    try:
        return %model_upper%Service.delete_one(%model_lower%_id)
    except (ApiError, ValueError, TypeError, exc.SQLAlchemyError) as error:
        return response.error(error)
