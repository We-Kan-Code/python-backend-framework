"""%model_upper% service"""
from http import HTTPStatus as http_status
from flask import request, current_app as app
from marshmallow import EXCLUDE
from src.app.%model_lower%.models.%model_lower% import %model_upper% as %model_upper%Model
from src.app.%model_lower%.schema.%model_lower% import %model_upper%Schema
from src.common.exceptions import ApiError
from src.common.helpers import (
    to_snake_case,
    validate_sparse_fieldsets,
    remove_empty_objects,
    build_pagination_meta)
import src.common.response as response
import src.resources.strings.%model_lower% as strings


class %model_upper%Service:
    """%model_upper% service class"""

    def __init__(self):
        self.%model_lower%_schema = %model_upper%Schema(unknown=EXCLUDE)
        self.%model_lower%s_schema = %model_upper%Schema(many=True)
        # Define relationships here
        # For eg. If the model has an attribute role that defines the 
        # relation (DB.Relationship) with the model Role, include it in 
        # the self.relations dict and link it to the Role Schema class 
        # you wish to use. 
        # self.relations = {'role': RoleSchemaClass}
        # Needed for sparse fieldsets and expand relations in GET calls
        self.relations = {}

    def create_one(self, request_body):
        """
        Create a single %model_lower%
        """
        %model_lower%_data = self.%model_lower%_schema.load(request_body)
        %model_lower% = %model_upper%Model(%model_lower%_data)
        %model_lower%.save()
        %model_lower% = self.%model_lower%_schema.dump(%model_lower%)

        return response.success(http_status.CREATED, '%model_lower%', %model_lower%)

    def update_one(self, %model_lower%_id, request_body):
        """
        Updates the %model_lower% if the %model_lower% exists. 
        """
        %model_lower%_data = self.%model_lower%_schema.load(request_body, partial=True)
        %model_lower%_data.pop('email', None)

        %model_lower% = %model_upper%Model.get_by_id(%model_lower%_id)
        if not %model_lower%:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        %model_lower%.update(%model_lower%_data)
        %model_lower% = self.%model_lower%_schema.dump(%model_lower%)

        return response.success(http_status.OK, '%model_lower%', %model_lower%)

    def get_one(self, %model_lower%_id, fields, relations):
        """
        Get a single %model_lower% based on the provided %model_lower%_id
        Returns a specific %model_lower% object.

        fields = Comma separated list of fields should be returned.
        relations = Relation and relation attributes that must be returned
        """
        valid_fields = set(%model_upper%Model.__dict__)
        fields = to_snake_case(fields).split(',')
        fields_to_return = [*(valid_fields & set(fields))]
        %model_lower%_schema = %model_upper%Schema(
            only=validate_sparse_fieldsets(
                fields_to_return, relations, self.relations))

        %model_lower% = %model_upper%Model.get_one(%model_lower%_id)
        if %model_lower% is None:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        %model_lower% = %model_lower%_schema.dump(%model_lower%)

        return response.success(http_status.OK, '%model_lower%', %model_lower%)

    def get_many(self, fields, relations, sort, page, per_page):
        """
        Get many / all %model_lower%s
        Returns an array of %model_lower%s.

        fields = Comma separated list of fields should be returned.
        relations = Relation and relation attributes that must be returned
        sort = Sorts the results based on the provided column in asc/desc order.
          Defaults to 'id|asc', sort by id in ascending order, if sort if None or invalid
        page = Provides pagination. Starts at 1. If zero (also the default if no value
          is provided), pagination is disabled and all results are returned

        per_page = No. of results to be returned in each page, defaults to 10
        """
        valid_fields = set(%model_upper%Model.__dict__)
        fields = to_snake_case(fields).split(',')
        fields_to_return = [*(valid_fields & set(fields))]
        original_%model_lower%s_schema = self.%model_lower%s_schema
        new_%model_lower%s_schema = %model_upper%Schema(
            many=True,
            only=validate_sparse_fieldsets(
                fields_to_return, relations, self.relations))
        self.%model_lower%s_schema = new_%model_lower%s_schema

        sort[0] = to_snake_case(sort[0])
        if sort[0] not in valid_fields:
            sort[0] = 'id'
            sort[1] = 'asc'

        %model_lower%s = %model_upper%Model.get_many(
            sort=sort,
            page=page,
            per_page=per_page)

        meta = None

        if page > 0:
            meta = build_pagination_meta(%model_lower%s, request)
            %model_lower%s = remove_empty_objects(%model_lower%s.items, self.%model_lower%s_schema)
        else:
            %model_lower%s = remove_empty_objects(%model_lower%s, self.%model_lower%s_schema)

        self.%model_lower%s_schema = original_%model_lower%s_schema

        return response.success(http_status.OK, '%model_lower%s', %model_lower%s, meta)

    @staticmethod
    def delete_one(%model_lower%_id):
        """Checks if the %model_lower% exists and deletes it"""
        %model_lower% = %model_upper%Model.query.get(%model_lower%_id)
        if not %model_lower%:
            raise ApiError(http_status.NOT_FOUND, strings.NOT_FOUND_ERROR)

        %model_upper%Model.delete_one(%model_lower%_id)

        return response.success(http_status.NO_CONTENT)
