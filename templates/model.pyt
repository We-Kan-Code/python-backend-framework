"""%model_upper% model"""
import datetime
from sqlalchemy import asc, desc
from src.server import DB


class %model_upper%(DB.Model):
    """%model_upper% table definition and supporting database operations"""

    __tablename__ = '%model_lower%s'

    id = DB.Column(DB.Integer, primary_key=True)
    created_at = DB.Column(DB.DateTime, default=datetime.datetime.utcnow())
    updated_at = DB.Column(DB.DateTime, default=datetime.datetime.utcnow())

    def __init__(self, data):
        # Init fields with the passed in dict data
        # For eg. self.column_name = data.get('column_name')
        pass

    # Database operations
    def save(self):
        """Creates a new %model_lower%"""
        DB.session.add(self)
        DB.session.commit()

    def update(self, data):
        """Updates an existing %model_upper%"""
        for key, item in data.items():
            if key in ('password', 'tmp_password'):
                item = self.generate_hash(item)
            setattr(self, key, item)
        self.updated_at = datetime.datetime.utcnow()
        DB.session.commit()

    @staticmethod
    def delete_one(%model_lower%_id):
        """Deletes a specific %model_lower% by id"""
        %model_upper%.query.filter_by(id=%model_lower%_id).delete()
        DB.session.commit()

    @staticmethod
    def get_many(sort, page=0, per_page=10):
        """
        Get many / all %model_lower%s
        If page is not a non-zero positive number, pagination is disabled
        """
        sort_by = desc if sort[1] == 'desc' else asc
        query = %model_upper%.query
        if page > 0:
            return query.order_by(sort_by(getattr(%model_upper%, sort[0]))).\
                paginate(per_page=per_page, page=page, error_out=False)
        return query.order_by(sort_by(getattr(%model_upper%, sort[0]))).all()

    @staticmethod
    def get_one(%model_lower%_id):
        """
        Get specific %model_lower% by id, supports sparse fieldsets
        Results contain only the requested columns
        """
        return %model_upper%.query.filter_by(id=%model_lower%_id).first()

    @staticmethod
    def get_by_id(%model_lower%_id):
        """Get specific %model_lower% by by"""
        return %model_upper%.query.filter_by(id=%model_lower%_id).first()
