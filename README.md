# Backend Framework (Python 3.6+)

## Introduction

A framework for building server-side applications in python. The framework sits on top of flask-restful and sqlalchemy and should easily integrate with other flask plugins.

## Getting stared

Checkout the [wiki](https://bitbucket.org/We-Kan-Code/python-backend-framework/wiki/Home) to get for information