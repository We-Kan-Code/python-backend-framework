""" Development task runner"""
import os
from logging import getLogger
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from src.configuration.modules.logger import RequestHandlerLoggerOverride
from src.server import create_app, DB
from src.common.constants import DEFAULT_PORT
from src.app.auth.models.role import Role as RoleModel
from src.app.auth.models.permission import Permission as PermissionModel
from src.app.user.models.user import User as UserModel
from storage.database.seed.data.roles import SEED_ROLES
from storage.database.seed.data.permissions import SEED_PERMISSIONS
from storage.database.seed.data.users import SEED_USERS


APP, CELERY = create_app()
APP.app_context().push()

MANAGER = Manager(APP)
Migrate(APP, DB, directory=os.path.join('./', 'storage/database/migrations'))
MANAGER.add_command('db', MigrateCommand)

LOGGER = getLogger(__name__)


def build_and_seed(model, seed_data):
    """Build entries and seed into tables"""
    session = DB.session()
    if session.query(model).count() > 0:
        return 'Stopped - Table not empty'
    entries = []
    for data in seed_data:
        entries.append(model(data))
    session.add_all(entries)
    session.commit()
    return 'Done'


@MANAGER.command
def seed():
    "Load initial data into database."
    LOGGER.info('Seeding Roles ... %s', build_and_seed(RoleModel, SEED_ROLES))
    LOGGER.info('Seeding Permissins ... %s', build_and_seed(
        PermissionModel, SEED_PERMISSIONS))
    LOGGER.info('Seeding Users ... %s', build_and_seed(UserModel, SEED_USERS))


@MANAGER.command
def run():
    """ Development init """
    port = os.getenv('PORT')
    enable_debug = (os.getenv('FLASK_DEBUG') == 'True')
    port = int(port) if port else DEFAULT_PORT

    APP.run(
        host='0.0.0.0',
        port=port,
        debug=enable_debug,
        use_debugger=enable_debug,
        use_reloader=enable_debug,
        threaded=True,
        request_handler=RequestHandlerLoggerOverride)


if __name__ == '__main__':
    MANAGER.run()
