"""Test helpers"""
from flask_jwt_extended import create_access_token, create_refresh_token

# Seed or create an admin user before testing
ADMIN_ID = 1
ADMIN_EMAIL_ADDRESS = 'admin@wekan.company'


# Auth helper
def create_jwt_access_token(id, email):
    """Creates jwt token"""
    access_token = create_access_token(
        identity={'id': ADMIN_ID, 'email': ADMIN_EMAIL_ADDRESS})
    return {
        'Authorization': 'Bearer {}'.format(access_token)
    }


def create_jwt_refresh_token(refresh_token):
    """Creates refresh token"""
    return {
        'Authorization': 'Bearer {}'.format(refresh_token)
    }
