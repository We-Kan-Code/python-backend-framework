"""Fixtures"""
import pytest
from sqlalchemy import event, create_engine
from src.server import create_app, DB as _db


def pytest_itemcollected(item):
    """Makes pytest use docstrings as test case descriptions"""
    par = item.parent.obj
    node = item.obj
    pref = par.__doc__.strip() if par.__doc__ else par.__class__.__name__
    suf = node.__doc__.strip() if node.__doc__ else node.__name__
    if pref or suf:
        item._nodeid = ' '.join(  # pylint:disable = protected-access
            (pref, suf))


@pytest.fixture(scope="session")
def app(request):  # pylint:disable = unused-argument
    """Returns session-wide application"""
    application, celery = create_app()  # pylint:disable = unused-variable
    return application


@pytest.fixture(scope="function", autouse=True)
def database_session(app, request):  # pylint:disable = unused-argument
    """Returns function-scoped session"""
    with app.app_context():
        app.config['SQLALCHEMY_ENGINE_OPTIONS'] = {
            'hide_parameters': True, 'convert_unicode': False}
        options = {'convert_unicode': False}
        engine = create_engine(
            app.config['SQLALCHEMY_DATABASE_URI'], **options)
        connection = engine.connect()
        transaction = connection.begin()

        options = dict(bind=connection)
        session = _db.create_scoped_session(options=options)
        session.begin_nested()

        @event.listens_for(session(), 'after_transaction_end')
        # pylint:disable = unused-argument
        def restart_savepoint(sess, trans):
            if trans.nested and not trans.parent.nested:
                session.expire_all()
                session.begin_nested()
        _db.session = session

        yield session

        # Cleanup
        session.remove()
        transaction.rollback()
        connection.close()
