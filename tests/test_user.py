"""[User tests]"""
# module conftest.py
from unittest.mock import patch
from http import HTTPStatus as http_status
from src.app.user.models.user import User as UserModel
from src.app.user.services.user import UserService

NEW_USER_INFO = {
    "firstName": "Sanchan",
    "lastName": "Moses",
    "email": "sanchanm112@wekancode.com",
    "password": "122ab7797a6868cc8621f61f8efe98823116134c3a314505e3cdb...",
    "roleId": 1}


# Integration tests
def test_user_create_negative_1(mocker, client):
    """[Integration] [POST /v1/users] With an empty request body returns validation errors"""
    empty_user_info = {}
    response = client.post('/v1/users', json=empty_user_info)
    assert response.status_code == http_status.UNPROCESSABLE_ENTITY
    assert 'errors' in response.json


def test_user_create_positive_1(mocker, client):
    """[Integration] [POST /v1/users] With valid data creates a user and returns user information"""
    # Avoid mail spam
    user_service = mocker.patch('src.app.user.services.user.UserService')
    response = client.post('/v1/users', json=NEW_USER_INFO)
    user_service.send_register_mail.assert_called_once()
    assert response.status_code == http_status.CREATED
    assert 'data' in response.json


# Unit tests
@patch('src.common.response.success')
@patch.object(UserService, 'send_register_mail')
@patch.object(UserModel, 'get_by_email')
@patch.object(UserModel, 'save')
def test_user_service_positive_1(user_save, get_by_email, send_register_mail, response_success):
    """[Unit] [UserService.create_one] On passing in valid json, a user is created"""
    service = UserService()
    get_by_email.return_value = None
    user_save.return_value = UserModel(NEW_USER_INFO)
    send_register_mail.return_value = None
    response_success.return_value = {}

    service.create_one(NEW_USER_INFO)
    user_save.assert_called_once()
    send_register_mail.assert_called_once()
    response_success.assert_called_once()
