"""[Auth tests]"""
# module conftest.py
from unittest.mock import patch
from http import HTTPStatus as http_status
from src.app.auth.models.role import Role as RoleModel
from src.app.auth.services.role import RoleService
from tests.helpers import create_jwt_access_token
from storage.database.seed.data.users import SEED_USERS as TEST_USER

TEST_USER = TEST_USER[0]

VALID_ROLE = {
    "name": "Test User Group",
    "status": "1"}


# Integration tests
def test_role_create_negative_1(mocker, client):
    """[Integration] [POST /v1/roles] With an empty request \
      body returns validation errors"""
    invalid_role = {}
    response = client.post('/v1/auth/users/login', json=TEST_USER)
    headers = create_jwt_access_token(
        response.json['data']['user']['id'], response.json['data']['user']['email'])
    response = client.post('/v1/roles', json=invalid_role, headers=headers)
    assert response.status_code == http_status.UNPROCESSABLE_ENTITY
    assert 'errors' in response.json


def test_role_create_positive_1(mocker, client):
    """[Integration] [POST /v1/roles] With valid data creates a role and returns \
      created role information"""
    response = client.post('/v1/auth/users/login', json=TEST_USER)
    headers = create_jwt_access_token(
        response.json['data']['user']['id'], response.json['data']['user']['email'])
    response = client.post('/v1/roles', json=VALID_ROLE, headers=headers)
    assert response.status_code == http_status.CREATED
    assert 'data' in response.json


# Unit tests
@patch('src.common.response.success')
@patch.object(RoleModel, 'get_by_name')
@patch.object(RoleModel, 'save')
def test_role_service_positive_1(role_save, get_by_name, response_success):
    """[Unit] [RoleService.create_one] On passing in valid json, \
      a user is created"""
    service = RoleService()
    get_by_name.return_value = None
    role_save.return_value = RoleModel(VALID_ROLE)
    response_success.return_value = {}

    service.create_one(VALID_ROLE)
    get_by_name.assert_called_once()
    role_save.assert_called_once()
    response_success.assert_called_once()
