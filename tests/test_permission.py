"""[Auth tests]"""
# module conftest.py
from unittest.mock import patch
from http import HTTPStatus as http_status
from src.app.auth.models.permission import Permission as PermissionModel
from src.app.auth.services.permission import PermissionService
from tests.helpers import create_jwt_access_token
from storage.database.seed.data.users import SEED_USERS as TEST_USER

TEST_USER = TEST_USER[0]

VALID_PERMISSION = {"resource": "users", "action": "delete:any",
                    "attributes": "*", "roleId": 1}


# Integration tests
def test_permission_create_negative_1(mocker, client):  # pylint:disable = unused-argument
    """[POST /v1/permissions] With an empty request \
      body returns validation errors"""
    invalid_permission = {}
    response = client.post('/v1/auth/users/login', json=TEST_USER)
    headers = create_jwt_access_token(
        response.json['data']['user']['id'], response.json['data']['user']['email'])
    response = client.post(
        '/v1/permissions', json=invalid_permission, headers=headers)
    assert response.status_code == http_status.UNPROCESSABLE_ENTITY
    assert 'errors' in response.json


def test_permission_create_positive_1(mocker, client):  # pylint:disable = unused-argument
    """[POST /v1/permissions] With valid data creates a permission and returns \
      created permission information"""
    response = client.post('/v1/auth/users/login', json=TEST_USER)
    headers = create_jwt_access_token(
        response.json['data']['user']['id'], response.json['data']['user']['email'])
    response = client.post(
        '/v1/permissions', json=VALID_PERMISSION, headers=headers)
    assert response.status_code == http_status.CREATED
    assert 'data' in response.json


# Unit tests
@patch('src.common.response.success')
@patch.object(PermissionModel, 'get_unique')
@patch.object(PermissionModel, 'save')
def test_permission_service_positive_1(
        permission_save,
        get_unique,
        response_success):
    """[PermissionService.create_one] On passing in valid json, \
      a user is created"""
    service = PermissionService()
    get_unique.return_value = None
    permission_save.return_value = PermissionModel(VALID_PERMISSION)
    response_success.return_value = {}

    service.create_one(VALID_PERMISSION)
    get_unique.assert_called_once()
    permission_save.assert_called_once()
    response_success.assert_called_once()
