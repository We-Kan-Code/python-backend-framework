"""[Auth tests]"""
# module conftest.py
from unittest.mock import patch
from http import HTTPStatus as http_status
from src.app.user.models.user import User as UserModel
from src.app.auth.services.auth import AuthService
from tests.helpers import create_jwt_refresh_token
from storage.database.seed.data.users import SEED_USERS as TEST_USER

TEST_USER = TEST_USER[0]
USER_LOGIN_ENDPOINT = '/v1/auth/users/login'


# Endpoint tests
# Login endpoints
def test_auth_login_negative_1(mocker, client):  # pylint:disable = unused-argument
    """[POST /v1/auth/users/login] Fails with \
validation errors when the request contains an empty json object"""
    request_body = {}
    response = client.post(USER_LOGIN_ENDPOINT, json=request_body)
    assert response.status_code == http_status.UNPROCESSABLE_ENTITY
    assert 'errors' in response.json


def test_auth_login_positive_1(mocker, client):
    """[POST /v1/auth/users/login] On successful \
login returns user information along with access and refresh tokens"""
    get_by_email = mocker.patch.object(UserModel, 'get_by_email')
    get_by_email.return_value = UserModel(TEST_USER)
    check_hash = mocker.patch.object(UserModel, 'check_hash')
    check_hash.return_value = True

    user_save = mocker.patch.object(UserModel, 'save')
    user_save.return_value = None

    response = client.post(USER_LOGIN_ENDPOINT, json=TEST_USER)
    assert response.status_code == http_status.OK
    assert 'data' in response.json


# Refresh endpoints
def test_auth_refresh_token_negative_1(mocker, client):  # pylint:disable = unused-argument
    """[POST /v1/auth/users/refresh] Fails with \
validation errors when the request contains an empty json object"""
    response = client.post('/v1/auth/users/refresh',
                           headers={})
    assert response.status_code == http_status.UNAUTHORIZED
    assert 'errors' in response.json


def test_auth_refresh_token_positive_1(mocker, client):
    """[POST /v1/auth/users/refresh] On successful \
refresh returns user information along with access and refresh tokens"""
    get_by_email = mocker.patch.object(UserModel, 'get_by_email')
    get_by_email.return_value = UserModel(TEST_USER)
    check_hash = mocker.patch.object(UserModel, 'check_hash')
    check_hash.return_value = True

    user_save = mocker.patch.object(UserModel, 'save')
    user_save.return_value = None

    response = client.post(USER_LOGIN_ENDPOINT, json=TEST_USER)
    response = client.post('/v1/auth/users/refresh',
                           headers=create_jwt_refresh_token(
                               response.json['data']['user']['refreshToken']))
    assert response.status_code == http_status.OK
    assert 'data' in response.json


# Unit tests
@patch('src.common.response.success')
@patch.object(UserModel, 'get_by_email')
@patch.object(UserModel, 'save')
@patch.object(UserModel, 'check_hash')
def test_auth_service_positive_1(
        check_hash,
        user_save,
        get_by_email,
        response_success):
    """[Service.auth] On passing valid data, the user \
is provided access and refresh tokens"""
    service = AuthService()
    get_by_email.return_value = UserModel(TEST_USER)
    response_success.return_value = {}

    service.auth(TEST_USER)
    check_hash.assert_called_once()
    user_save.assert_called_once()
    response_success.assert_called_once()
